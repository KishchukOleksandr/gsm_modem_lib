#ifndef _IGSM_TRANSMITTER_H__
#define _IGSM_TRANSMITTER_H__
#include <string>
#include <vector>

class IGSMTransmitter
{
public:
  virtual void Read(std::string &data) = 0;
  virtual void ReadBinary(std::vector<uint8_t> &data) = 0;
  virtual void Write(const std::string &data) = 0;
  virtual void WriteBinary(const std::vector<uint8_t> &data) = 0;
};
#endif // _IGSM_TRANSMITTER_H__
