#include "IGSMTransmitter.hpp"
#include <thread>
#include <string>
#include <vector>

using GSMMessageCallBack = void (*)(std::string &);

struct GSMQueueItem
{
  std::string message;
  GSMMessageCallBack cb = nullptr;
  int timeout = 30;
  int retryCnt = 10;
};

class GSM
{
private:
  IGSMTransmitter &trx;
  std::vector<GSMQueueItem> queue;
  std::thread messageQueueThread;
  bool exitFromThread{false};

public:
  GSM(IGSMTransmitter &trx);
  ~GSM();
  void Init(void);

private:
  void CheckMessageQueue(void);
  void AddMessageItem(const GSMQueueItem &message);
  void DelMessageItem(void);

public:
  static void ATCommandCallBack(std::string &answer);
};
