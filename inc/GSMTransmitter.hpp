#include "IGSMTransmitter.hpp"
#include "RPiUart.hpp"
#include <string>

class GSMtransmitter : public IGSMTransmitter
{
public:
  GSMtransmitter(const std::string &name, uint baudrate);
  ~GSMtransmitter();

  void Read(std::string &data) override;
  void ReadBinary(std::vector<uint8_t> &data) override;
  void Write(const std::string &data) override;
  void WriteBinary(const std::vector<uint8_t> &data) override;

private:
  RPiUart *uart;
  RPiUart::BaudRate br;
  std::string name;
};