#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <sys/mman.h>
#include "GSM.hpp"
#include "GSMTransmitter.hpp"
#include <thread>
#include <chrono>

int main(int argv, char* argc[])
{
  GSMtransmitter transmitter {"/dev/ttyAMA0", 115200};
  GSM modem {transmitter};
  modem.Init();

  std::this_thread::sleep_for(std::chrono::milliseconds(5000));
  
  return 0;
}
