
#include "GSM.hpp"
#include <iostream>
#include <chrono>

const GSMQueueItem AT{"AT", &GSM::ATCommandCallBack};
const GSMQueueItem ATI{"ATI", &GSM::ATCommandCallBack};

GSM::GSM(IGSMTransmitter &trx) : trx{trx}
{
  messageQueueThread = std::thread{&GSM::CheckMessageQueue, this};
}

GSM::~GSM()
{
  exitFromThread = true;
  messageQueueThread.join();
  std::cout << "GSM Destrioed\r\n";
}

void GSM::Init(void)
{
  AddMessageItem(AT);
  AddMessageItem(ATI);
}

void GSM::CheckMessageQueue(void)
{
  std::string answer;

  for (;;)
  {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    if (queue.size() > 0)
    {
      this->trx.Write(static_cast<const std::string>(queue.at(0).message + "\r\n"));
      std::cout << queue.at(0).message << "\r\n";
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
      this->trx.Read(answer);
      if (queue.at(0).cb != nullptr)
      {
        queue.at(0).cb(answer);
        DelMessageItem();
        answer.clear();
      }
    }
    if (exitFromThread)
      break;
  }
}

void GSM::AddMessageItem(const GSMQueueItem &message)
{
  this->queue.push_back(message);
}

void GSM::DelMessageItem(void)
{
  this->queue.erase(queue.begin());
}

void GSM::ATCommandCallBack(std::string &answer)
{
  std::cout <<  answer;
}
