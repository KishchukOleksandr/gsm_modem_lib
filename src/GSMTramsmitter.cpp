#include "GSMTransmitter.hpp"

GSMtransmitter::GSMtransmitter(const std::string &name, uint baudrate)
{
  switch (baudrate)
  {
  case 0:
    br = RPiUart::BaudRate::BR_0;
    break;
  case 1200:
    br = RPiUart::BaudRate::BR_1200;
    break;
  case 2400:
    br = RPiUart::BaudRate::BR_2400;
    break;
  case 4800:
    br = RPiUart::BaudRate::BR_4800;
    break;
  case 9600:
    br = RPiUart::BaudRate::BR_9600;
    break;
  case 19200:
    br = RPiUart::BaudRate::BR_19200;
    break;
  case 38400:
    br = RPiUart::BaudRate::BR_38400;
    break;
  case 57600:
    br = RPiUart::BaudRate::BR_57600;
    break;
  case 115200:
    br = RPiUart::BaudRate::BR_115200;
    break;
  case 230400:
    br = RPiUart::BaudRate::BR_230400;
    break;
  case 460800:
    br = RPiUart::BaudRate::BR_460800;
    break;
  case 921600:
    br = RPiUart::BaudRate::BR_921600;
    break;
  default:
    br = RPiUart::BaudRate::BR_115200;
    break;
  }

  this->name = name;
  uart = new RPiUart(this->name, br);
}

GSMtransmitter::~GSMtransmitter()
{

}

void GSMtransmitter::Read(std::string &data)
{
  uart->Read(data);
}

void GSMtransmitter::ReadBinary(std::vector<uint8_t> &data)
{
}
void GSMtransmitter::Write(const std::string &data)
{
  uart->Write(data);
}
void GSMtransmitter::WriteBinary(const std::vector<uint8_t> &data)
{
}